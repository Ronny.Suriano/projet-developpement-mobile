Fonctionnalités implémentées :

1. Affiche la météo d'une ville avec l'API CurrentWeather
Ville / Temps / Température 

2. Affiche la météo sur 5 jours avec l'API Forecast5
Jour de la semaine / Temps du matin / Température du matin / Temps de l'après midi / Température de l'après midi

3. Permet de rechercher une ville pour afficher sa météo
- à l'aide d'une barre de recherche
- via géolocalisation (il faut au préalable choisir une géolocalisation dans l'émulateur)
- affichage ville inconnue quand la ville recherchée n'existe pas et accès aux prévisions desactivées

4. Permet d'ajouter/supprimer une ville en favoris avec des SharedPreferences
- persistances des données
- ajout/supression depuis l'affichage de la météo de la ville
- suppression depuis l'onglet favoris
- météo des villes favorites consultables depuis l'onglet favoris + redirection vers les villes

5. Splashscreen



