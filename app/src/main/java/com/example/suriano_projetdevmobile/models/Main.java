package com.example.suriano_projetdevmobile.models;

import com.google.gson.annotations.SerializedName;

public class Main {
    @SerializedName("temp")
    private Float temp;
    @SerializedName("feels_like")
    private Float feels_like;

    public Main(float temp,float feels_like){
        this.temp=temp;
        this.feels_like=feels_like;
    }

    public Float getTemp() {
        return temp;
    }

    public void setTemp(float temp) {
        this.temp = temp;
    }

    public Float getFeels_like() {
        return feels_like;
    }

    public void setFeels_like(float feels_like) {
        this.feels_like = feels_like;
    }
}
