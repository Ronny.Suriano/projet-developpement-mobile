package com.example.suriano_projetdevmobile.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CurrentWeather {
    @SerializedName("weather")
    private ArrayList<Weather> weather;
    @SerializedName("main")
    private Main main;
    @SerializedName("name")
    private String name;
    @SerializedName("sys")
    private Sys sys;

    public CurrentWeather(){}

    public CurrentWeather(ArrayList<Weather> weather,Main main, String name, Sys sys){
        this.weather=weather;
        this.main=main;
        this.name=name;
        this.sys = sys;
    }

    public ArrayList<Weather> getWeather() {
        return weather;
    }

    public void setWeather(ArrayList<Weather> weather) {
        this.weather = weather;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }
}
