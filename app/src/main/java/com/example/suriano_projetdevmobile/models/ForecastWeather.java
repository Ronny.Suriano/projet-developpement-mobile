package com.example.suriano_projetdevmobile.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ForecastWeather {

    @SerializedName("list")
    private List<HourWeather> hourWeather;
    @SerializedName("city")
    private City city;

    public ForecastWeather(List<HourWeather> hourWeather, City city){
        this.hourWeather=hourWeather;
        this.city=city;
    }

    public List<HourWeather> getHourWeather() {
        return hourWeather;
    }

    public void setHourWeather(List<HourWeather> hourWeather) {
        this.hourWeather = hourWeather;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
