package com.example.suriano_projetdevmobile.models;

public class Prevision {
    private String dt_txt;
    private String morning_icon;
    private Float morning_temp;
    private String afternoon_icon;
    private Float afternoon_temp;

    public Prevision(String dt_txt,String morning_icon,Float morning_temp, String afternoon_icon, Float afternoon_temp){
        this.dt_txt=dt_txt;
        this.morning_icon=morning_icon;
        this.morning_temp=morning_temp;
        this.afternoon_icon=afternoon_icon;
        this.afternoon_temp=afternoon_temp;
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public void setDt_txt(String dt_txt) {
        this.dt_txt = dt_txt;
    }

    public String getMorning_icon() {
        return morning_icon;
    }

    public void setMorning_icon(String morning_icon) {
        this.morning_icon = morning_icon;
    }

    public Float getMorning_temp() {
        return morning_temp;
    }

    public void setMorning_temp(Float morning_temp) {
        this.morning_temp = morning_temp;
    }

    public String getAfternoon_icon() {
        return afternoon_icon;
    }

    public void setAfternoon_icon(String afternoon_icon) {
        this.afternoon_icon = afternoon_icon;
    }

    public Float getAfternoon_temp() {
        return afternoon_temp;
    }

    public void setAfternoon_temp(Float afternoon_temp) {
        this.afternoon_temp = afternoon_temp;
    }
}
