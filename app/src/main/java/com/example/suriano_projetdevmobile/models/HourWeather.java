package com.example.suriano_projetdevmobile.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;

public class HourWeather {
    @SerializedName("main")
    private Main main;
    @SerializedName("weather")
    private ArrayList<Weather> weather;
    @SerializedName("dt_txt")
    private String dt_txt;

    public HourWeather(Main main, ArrayList<Weather> weather,String dt_txt){
        this.main=main;
        this.weather=weather;
        this.dt_txt=dt_txt;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public ArrayList<Weather> getWeather() {
        return weather;
    }

    public void setWeather(ArrayList<Weather> weather) {
        this.weather = weather;
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public void setDt_txt(String dt_txt) {
        this.dt_txt = dt_txt;
    }
}
