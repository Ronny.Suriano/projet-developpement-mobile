package com.example.suriano_projetdevmobile.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.suriano_projetdevmobile.Activities.MainActivity;
import com.example.suriano_projetdevmobile.R;
import com.example.suriano_projetdevmobile.RecyclerViews.RecyclerViewAdapterFavorites;
import com.google.android.material.bottomnavigation.BottomNavigationView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FavorisFragment} factory method to
 * create an instance of this fragment.
 */
public class FavorisFragment extends Fragment {

    RecyclerView recyclerView;

    public FavorisFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favoris, container, false);

        // check des items de la bottom nav bar
        BottomNavigationView bottomNavigationView = (BottomNavigationView) requireActivity().findViewById(R.id.activity_main_bottom_navigation);
        bottomNavigationView.getMenu().setGroupCheckable(0, true, true);
        bottomNavigationView.getMenu().getItem(1).setChecked(true);

        // initialisation du recyclerview et de son adapter
        recyclerView = view.findViewById(R.id.recyclerView_favs);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));

        RecyclerViewAdapterFavorites adapter = new RecyclerViewAdapterFavorites(((MainActivity) getActivity()).favoritesList,requireContext());
        recyclerView.setAdapter(adapter);

        return view;
    }
}