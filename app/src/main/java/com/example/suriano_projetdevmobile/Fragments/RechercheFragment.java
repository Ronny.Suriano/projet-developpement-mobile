package com.example.suriano_projetdevmobile.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.suriano_projetdevmobile.Activities.MainActivity;
import com.example.suriano_projetdevmobile.R;
import com.example.suriano_projetdevmobile.RetrofitClient;
import com.example.suriano_projetdevmobile.WeatherService;
import com.example.suriano_projetdevmobile.models.CurrentWeather;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RechercheFragment} factory method to
 * create an instance of this fragment.
 */
public class RechercheFragment extends Fragment{

    public Button buttonPrevisions;
    private String apiKey;
    private CurrentWeather currentWeatherResponse;
    public TextView city_name;
    public TextView city_weather;
    public TextView city_temp;
    public ImageView city_icon;
    public SearchView searchView;
    public ImageButton buttonFavorite;
    public ImageButton buttonLocation;
    public Location currentLocation;
    public LocationManager mLocationManager;

    public RechercheFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        apiKey = "91de76b291fab2ef388693e4290670f9";

        currentWeatherResponse = new CurrentWeather();

        // Get the intent, verify the action and get the query
        Intent intent = requireActivity().getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            fetchCurrentWeatherData(query,"fr","metric");
        }else{
            fetchCurrentWeatherData(((MainActivity)getActivity()).currentCity,"fr","metric");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recherche, container, false);

        // bouton de previsions
        buttonPrevisions = (Button) view.findViewById(R.id.button_previsions);
        buttonPrevisions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).onClick(new Previsions5joursFragment());
            }
        });

        // check des items de la bottom nav bar
        BottomNavigationView bottomNavigationView = (BottomNavigationView) requireActivity().findViewById(R.id.activity_main_bottom_navigation);
        bottomNavigationView.getMenu().setGroupCheckable(0, true, true);


        // recuperation des elements de la vue à afficher
        city_icon = (ImageView) view.findViewById(R.id.city_icon_weather);
        city_name = (TextView) view.findViewById(R.id.city_name);
        city_temp = (TextView) view.findViewById(R.id.city_temp);
        city_weather = (TextView) view.findViewById(R.id.city_weather);

        // search bar
        SearchManager searchManager = (SearchManager) requireActivity().getSystemService(requireContext().SEARCH_SERVICE);
        searchView = (SearchView) view.findViewById(R.id.searchview_bar);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(requireActivity().getComponentName()));
        searchView.setIconifiedByDefault(false);

        //button location
        buttonLocation = (ImageButton) view.findViewById(R.id.location_button);
        buttonLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get current location
                if (ContextCompat.checkSelfPermission(requireContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(requireContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    currentLocation = getLastKnownLocation();
                }else{
                    ActivityCompat.requestPermissions(requireActivity(),new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},101);
                }
                double latitude=0.0;
                double longitude=0.0;
                // si localisation pas encore à jour dans le cache on recupere les coordonnées de Grenoble
                if(currentLocation == null){
                    latitude = 45.0833;
                    longitude = 5.8333;
                }// sinon on utilise les coordonnées de la localisation
                else{
                    latitude = currentLocation.getLatitude();
                    longitude = currentLocation.getLongitude();
                }

                fetchCurrentWeatherDataWithLocation(""+latitude,""+longitude,"fr","metric");
            }
        });

        //button favorite
        buttonFavorite = (ImageButton) view.findViewById(R.id.favorite_button);
        setupFavoriteIcon();
        buttonFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFavorite = checkIfCityIsFavorite(currentWeatherResponse.getName());
                if(!isFavorite){
                    // transformation en string des informations interressante pour l'ajout aux favoris
                    String strFavorite = currentWeatherResponse.getName()+"/"+currentWeatherResponse.getWeather().get(0).getIcon()+"/"+currentWeatherResponse.getMain().getTemp();
                    addToFavorite(strFavorite);
                }else{
                    removeFavorite(currentWeatherResponse.getName());
                }
            }
        });
        // Inflate the layout for this fragment
        return view;
    }


    private Location getLastKnownLocation() {
        mLocationManager = (LocationManager) requireActivity().getSystemService(requireContext().LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            @SuppressLint("MissingPermission") Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }


    public void fetchCurrentWeatherData(String cityName,String lang,String units){
        WeatherService weatherService = RetrofitClient.getInstance().create(WeatherService.class);
        weatherService.getCurrentWeather(cityName,lang,units,apiKey).enqueue(new Callback<CurrentWeather>() {
            @Override
            public void onResponse(Call<CurrentWeather> call, Response<CurrentWeather> response) {
                if(response.isSuccessful() && response.body() != null){
                    currentWeatherResponse = response.body();
                    String imgURL = "https://openweathermap.org/img/wn/" + currentWeatherResponse.getWeather().get(0).getIcon() + "@2x.png";
                    Picasso.with(requireContext()).load(imgURL).into(city_icon);
                    city_name.setText(currentWeatherResponse.getName());
                    city_temp.setText(Math.round(currentWeatherResponse.getMain().getTemp())+"°C");
                    city_weather.setText(currentWeatherResponse.getWeather().get(0).getMain());
                    ((MainActivity)requireContext()).currentCity=currentWeatherResponse.getName();
                    setupFavoriteIcon();
                }else{
                    Toast.makeText(requireContext(), "Oops something went wrong", Toast.LENGTH_SHORT).show();
                    city_icon.setImageDrawable(getResources().getDrawable(R.drawable.iconeapp));
                    city_name.setText("Ville inconnue");
                    city_temp.setText("XX °C");
                    city_weather.setText("Temps");
                    buttonPrevisions.setEnabled(false);
                }
            }

            @Override
            public void onFailure(Call<CurrentWeather> call, Throwable t) {
                //manage errors
            }
        });

    }

    private void fetchCurrentWeatherDataWithLocation(String latitude,String longitude,String lang,String units){
        WeatherService weatherService = RetrofitClient.getInstance().create(WeatherService.class);
        weatherService.getCurrentWeatherWithLocation(latitude,longitude,lang,units,apiKey).enqueue(new Callback<CurrentWeather>() {
            @Override
            public void onResponse(Call<CurrentWeather> call, Response<CurrentWeather> response) {
                if(response.isSuccessful() && response.body() != null){
                    currentWeatherResponse = response.body();
                    String imgURL = "https://openweathermap.org/img/wn/" + currentWeatherResponse.getWeather().get(0).getIcon() + "@2x.png";
                    Picasso.with(requireContext()).load(imgURL).into(city_icon);
                    city_name.setText(currentWeatherResponse.getName());
                    city_temp.setText(Math.round(currentWeatherResponse.getMain().getTemp())+"°C");
                    city_weather.setText(currentWeatherResponse.getWeather().get(0).getMain());
                    setupFavoriteIcon();
                }else{
                    Toast.makeText(requireContext(), "Oops something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CurrentWeather> call, Throwable t) {
                //manage errors
            }
        });

    }

    public void addToFavorite(String favorite){
        ((MainActivity) requireActivity()).favoritesList.add(favorite);
        ((MainActivity)requireActivity()).saveFavorites();
        setupFavoriteIcon();

    }

    public void removeFavorite(String cityname){
        ArrayList<String> favoris = ((MainActivity)requireActivity()).favoritesList;
        for(int i=0;i<favoris.size();i++){
            String[] parts = favoris.get(i).split("/");
            String name = parts[0];
            if(name.equals(cityname)){
                ((MainActivity)requireActivity()).favoritesList.remove(i);
            }
        }
        ((MainActivity) requireActivity()).saveFavorites();
        setupFavoriteIcon();
    }

    public boolean checkIfCityIsFavorite(String cityname){
        boolean isfavorite=false;
        ArrayList<String> favoris = ((MainActivity)requireActivity()).favoritesList;
        for(int i=0;i<favoris.size();i++){
            String[] parts = favoris.get(i).split("/");
            String name = parts[0];
            if(name.equals(cityname)){
                isfavorite=true;
            }
        }
        return isfavorite;
    }

    public void setupFavoriteIcon(){
        boolean isFavorite = checkIfCityIsFavorite(currentWeatherResponse.getName());
        if(isFavorite){
            Drawable tempImage = getResources().getDrawable(R.drawable.ic_baseline_star_24_yellow);
            buttonFavorite.setImageDrawable(tempImage);
        }
        else if(city_name.getText().equals("Ville inconnue")){
            //nothing
        }
        else{
            Drawable tempImage = getResources().getDrawable(R.drawable.ic_baseline_star_border_24_yellow);
            buttonFavorite.setImageDrawable(tempImage);
        }
    }

}