package com.example.suriano_projetdevmobile.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.suriano_projetdevmobile.Activities.MainActivity;
import com.example.suriano_projetdevmobile.R;
import com.example.suriano_projetdevmobile.RecyclerViews.RecyclerViewAdapter;
import com.example.suriano_projetdevmobile.RetrofitClient;
import com.example.suriano_projetdevmobile.WeatherService;
import com.example.suriano_projetdevmobile.models.ForecastWeather;
import com.example.suriano_projetdevmobile.models.HourWeather;
import com.example.suriano_projetdevmobile.models.Prevision;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Previsions5joursFragment} factory method to
 * create an instance of this fragment.
 */
public class Previsions5joursFragment extends Fragment {

    private String apiKey;
    ForecastWeather forecastWeatherResponse;
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;
    ArrayList<Prevision> previsions;

    public Previsions5joursFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        apiKey = "91de76b291fab2ef388693e4290670f9";

        previsions = new ArrayList<>();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_previsions5jours, container, false);

        // uncheck des items de la bottom nav bar
        BottomNavigationView bottomNavigationView = (BottomNavigationView) requireActivity().findViewById(R.id.activity_main_bottom_navigation);
        bottomNavigationView.getMenu().setGroupCheckable(0, false, true);

        // initialisation du recyclerview et de l'adapter
        recyclerView = view.findViewById(R.id.recyclerView_prev);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        adapter = new RecyclerViewAdapter(new ArrayList<>(),getContext());
        recyclerView.setAdapter(adapter);

        // recupération des données de prevision de l'api
        fetchForecastData(((MainActivity)getActivity()).currentCity,"fr","metric");

        return view;
    }

    private void fetchForecastData(String cityName, String lang, String units){
        WeatherService weatherService = RetrofitClient.getInstance().create(WeatherService.class);
        weatherService.getForecastWeather(cityName,lang,units,apiKey).enqueue(new Callback<ForecastWeather>() {
            @Override
            public void onResponse(Call<ForecastWeather> call, Response<ForecastWeather> response) {
                if(response.isSuccessful() && response.body() != null){
                    //manage data
                    forecastWeatherResponse = response.body();
                    List<HourWeather> listHW = forecastWeatherResponse.getHourWeather();

                    for(int i=0;i<listHW.size();i++){
                        Prevision prevision = new Prevision("test","test_icon",new Float(0.0),"test_icon2",new Float(0.0));
                        if(listHW.get(i).getDt_txt().contains("09:00:00")){
                            prevision.setDt_txt(listHW.get(i).getDt_txt());
                            prevision.setMorning_icon(listHW.get(i).getWeather().get(0).getIcon());
                            prevision.setMorning_temp(listHW.get(i).getMain().getTemp());
                            if(i<=37){
                                prevision.setAfternoon_icon(listHW.get(i+2).getWeather().get(0).getIcon());
                                prevision.setAfternoon_temp(listHW.get(i+2).getMain().getTemp());
                            }else{
                                prevision.setAfternoon_icon(listHW.get(i).getWeather().get(0).getIcon());
                                prevision.setAfternoon_temp(listHW.get(i).getMain().getTemp());
                            }
                            previsions.add(prevision);
                        }
                    }
                    adapter.addList(previsions);
                }else{
                    Toast.makeText(getContext(),"Oops something went wrong",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ForecastWeather> call, Throwable t) {
                //manage errors
                Log.e("debug taille","bonjour nous voici dans le logcat");
            }
        });
    }
}