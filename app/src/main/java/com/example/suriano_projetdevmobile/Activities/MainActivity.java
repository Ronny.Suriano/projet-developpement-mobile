package com.example.suriano_projetdevmobile.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.suriano_projetdevmobile.Fragments.FavorisFragment;
import com.example.suriano_projetdevmobile.Fragments.RechercheFragment;
import com.example.suriano_projetdevmobile.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;

    public static String SHARED_PREFERENCES = "SharedPreferencesFavorites";
    public static final String FAVORITES = "Favorites";
    public ArrayList<String> favoritesList;
    public String currentCity;

    private void configureBottomView(){
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> updateMainFragment(item.getItemId()));
    }

    public Boolean updateMainFragment(Integer integer){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        switch (integer) {
            case R.id.action_recherche:
                fragmentTransaction.replace(R.id.fragment_container_view, new RechercheFragment());
                fragmentTransaction.commit();
                break;
            case R.id.action_favoris:
                loadFavorites();
                fragmentTransaction.replace(R.id.fragment_container_view, new FavorisFragment());
                fragmentTransaction.commit();
                break;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.ThemeLight);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        currentCity = "Grenoble";
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragment_container_view, new RechercheFragment());
        fragmentTransaction.commit();

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.activity_main_bottom_navigation);
        configureBottomView();

        favoritesList = new ArrayList<String>();
        loadFavorites();
    }

    public void onClick(Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container_view, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent myIntent = new Intent(MainActivity.this, SettingsActivity.class);
            MainActivity.this.startActivity(myIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // save favorites with sharedpreferences
    public void saveFavorites(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES,MODE_PRIVATE);
        SharedPreferences.Editor editorPreferences = sharedPreferences.edit();
        Set<String> setPreferences = new HashSet<String>(this.favoritesList);
        editorPreferences.putStringSet(FAVORITES,setPreferences);
        editorPreferences.commit();
    }


    // load favorites
    public void loadFavorites(){
        ArrayList<String> favoritesLocalList ;
        Set<String> listSet;
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES, MODE_PRIVATE);
        listSet = sharedPreferences.getStringSet(FAVORITES, Collections.emptySet());
        favoritesLocalList = new ArrayList<>(listSet);
        this.favoritesList = favoritesLocalList;
    }

    public void removeFavoriteFromFavoritesPage(String cityname){
        for(int i=0;i<favoritesList.size();i++){
            String[] parts = favoritesList.get(i).split("/");
            String name = parts[0];
            if(name.equals(cityname)){
                favoritesList.remove(i);
            }
        }
        saveFavorites();
    }


}