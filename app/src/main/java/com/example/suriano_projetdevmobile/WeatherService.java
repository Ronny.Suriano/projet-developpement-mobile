package com.example.suriano_projetdevmobile;

import com.example.suriano_projetdevmobile.models.CurrentWeather;
import com.example.suriano_projetdevmobile.models.ForecastWeather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WeatherService {

    @GET("data/2.5/weather")
    Call<CurrentWeather> getCurrentWeather(@Query("q")String name_country,@Query("lang")String lang,@Query("units")String units,@Query("appid")String apiKey);

    @GET("data/2.5/forecast")
    Call<ForecastWeather> getForecastWeather(@Query("q")String name_country, @Query("lang")String lang, @Query("units")String units, @Query("appid")String apiKey);

    @GET("data/2.5/weather")
    Call<CurrentWeather> getCurrentWeatherWithLocation(@Query("lat")String latitude,@Query("lon")String longitude,@Query("lang")String lang,@Query("units")String units,@Query("appid")String apiKey);
}
