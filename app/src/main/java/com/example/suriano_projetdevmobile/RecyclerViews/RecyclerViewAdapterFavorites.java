package com.example.suriano_projetdevmobile.RecyclerViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.suriano_projetdevmobile.Activities.MainActivity;
import com.example.suriano_projetdevmobile.Fragments.FavorisFragment;
import com.example.suriano_projetdevmobile.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RecyclerViewAdapterFavorites extends RecyclerView.Adapter<RecyclerViewAdapterFavorites.ViewHolder> {

    private ArrayList<String> favorites;
    private Context context;

    public RecyclerViewAdapterFavorites(ArrayList<String> favorites, Context context){
        this.favorites=favorites;
        this.context=context;
    }

    @Override
    public int getItemCount(){return this.favorites.size();}

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout_favorites,parent,false);
        return new RecyclerViewAdapterFavorites.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position){
        String item = favorites.get(position);

        String[] parts = item.split("/");
        String cityname = parts[0];
        String icon = parts[1];
        Float temp = Float.parseFloat(parts[2]);

        holder.itemCityname.setText(cityname);
        holder.itemCityname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)context).currentCity=cityname;
                BottomNavigationView bottomNavigationView = ((MainActivity)context).findViewById(R.id.activity_main_bottom_navigation);
                ((MainActivity) context).getIntent().setAction("");
                ((MainActivity) context).findViewById(R.id.action_recherche).performClick();
            }
        });


        String imgURL = "https://openweathermap.org/img/wn/" + icon + "@2x.png";
        Picasso.with(context).load(imgURL).into(holder.itemIcon);

        //traitement de format pour l'affichage
        if(temp<10.0){
            holder.itemTemp.setText("  "+Math.round(temp)+"°C");
        }else{holder.itemTemp.setText(Math.round(temp)+"°C");}

        holder.itemButtonFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)context).removeFavoriteFromFavoritesPage(cityname);
                ((MainActivity)context).onClick(new FavorisFragment());
            }
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        final Button itemCityname;
        final ImageView itemIcon;
        final TextView itemTemp;
        final ImageButton itemButtonFav;

        ViewHolder(View view){
            super(view);
            itemCityname = view.findViewById(R.id.item_favorite_cityname);
            itemIcon =  view.findViewById(R.id.item_favorite_icon);
            itemTemp = view.findViewById(R.id.item_favorite_temp);
            itemButtonFav = view.findViewById(R.id.item_button_fav);
        }
    }

    public void addListe(ArrayList<String> list){
        this.favorites = list;
    }

}
