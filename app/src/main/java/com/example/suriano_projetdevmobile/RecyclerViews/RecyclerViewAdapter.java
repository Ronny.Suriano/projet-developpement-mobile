package com.example.suriano_projetdevmobile.RecyclerViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.example.suriano_projetdevmobile.R;
import com.example.suriano_projetdevmobile.models.Prevision;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private ArrayList<Prevision> previsions;
    private Context context;

    public RecyclerViewAdapter(ArrayList<Prevision> previsions, Context context){
        this.previsions = previsions;
        this.context=context;
    }

    @Override
    public int getItemCount(){
        return this.previsions.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout,parent,false);
        return new RecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position){
        Prevision item = previsions.get(position);
        Date d = new Date("01/01/2001");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            d = sdf.parse(item.getDt_txt());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.itemDay.setText(item.getDt_txt());

       int item_day = d.getDay();
        switch (item_day){
            case 0: holder.itemDay.setText("Dimanche");
                    break;
            case 1: holder.itemDay.setText("Lundi");
                    break;
            case 2: holder.itemDay.setText("Mardi");
                    break;
            case 3: holder.itemDay.setText("Mercredi");
                    break;
            case 4: holder.itemDay.setText("Jeudi");
                    break;
            case 5: holder.itemDay.setText("Vendredi");
                    break;
            case 6: holder.itemDay.setText("Samedi");
                    break;
        }


        String imgURL = "https://openweathermap.org/img/wn/" + item.getMorning_icon() + "@2x.png";
        Picasso.with(context).load(imgURL).into(holder.itemMorningIcon);

        //on remplit les textview de la temperature du matin avec un traitement pour l'affichage
        if(item.getMorning_temp()<10.0){
            holder.itemMorningTemp.setText("  "+Math.round(item.getMorning_temp())+"°C");
        }else{holder.itemMorningTemp.setText(""+Math.round(item.getMorning_temp())+"°C");}


        imgURL = "https://openweathermap.org/img/wn/" + item.getAfternoon_icon() + "@2x.png";
        Picasso.with(context).load(imgURL).into(holder.itemAfternoonIcon);

        //on remplit les textview de la temperature de l'après midi avec un traitement pour l'affichage
        if(item.getAfternoon_temp()<10.0){
            holder.itemAfternoonTemp.setText("  "+Math.round(item.getAfternoon_temp())+"°C");
        }else{holder.itemAfternoonTemp.setText(Math.round(item.getAfternoon_temp())+"°C");}


    }

    class ViewHolder extends RecyclerView.ViewHolder{
        final TextView itemDay;
        final ImageView itemMorningIcon;
        final TextView itemMorningTemp;
        final ImageView itemAfternoonIcon;
        final TextView itemAfternoonTemp;

        ViewHolder (View view){
            super(view);
            itemDay = view.findViewById(R.id.item_day);
            itemMorningIcon = view.findViewById(R.id.item_morning_icon);
            itemMorningTemp = view.findViewById(R.id.item_morning_temp);
            itemAfternoonIcon = view.findViewById(R.id.item_afternoon_icon);
            itemAfternoonTemp = view.findViewById(R.id.item_afternoon_temp);
        }
    }

    public void addList(ArrayList<Prevision> list){
        this.previsions =list;
        notifyDataSetChanged();
    }

}
